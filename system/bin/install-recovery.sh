#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/mmcblk0p10:8050688:26eaa3d9b97ee134f5643cc1846feccc63b32439; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/mmcblk0p9:7620608:2c2e8dc85f5860cdd9b52e6887805a99f0349fb0 EMMC:/dev/block/mmcblk0p10 26eaa3d9b97ee134f5643cc1846feccc63b32439 8050688 2c2e8dc85f5860cdd9b52e6887805a99f0349fb0:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
